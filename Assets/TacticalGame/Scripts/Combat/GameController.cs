﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace TacticalGame
{
    [System.Serializable]
    class SaveCharacterInfo
    {
        public Vector2 position;
        public int id;

        public SaveCharacterInfo(Vector2 _position, int _id)
        {
            position = _position;
            id = _id;
        }
    }

    [System.Serializable]
    class SaveCharacterList
    {
        public List<SaveCharacterInfo> save; 
        public SaveCharacterList()
        {
            save = new List<SaveCharacterInfo>();
        }
    }

    public class GameController : NetworkBehaviour
    {
        public int mapWidth;
        public int mapHeight;

        [SyncVar]
        private bool selectCharState = true;
        [SyncVar]
        private bool moveState = false;
        GameServer.GameServer gameServer;

        Dictionary<int, FighterController> characters = new Dictionary<int, FighterController>();
        Dictionary<int, GameServer.Character> logicCharacters = new Dictionary<int, GameServer.Character>();

        [SyncVar]
        int selectedCharacter = -1;
        void Start()
        {
            if (isServer)
            {
                gameServer = new GameServer.GameServer(mapHeight, mapWidth);

                initTiles();
                initCharactersForHost();
            }
            else
            {
                initCharactersForClient();
                GetComponent<NetworkIdentity>().AssignClientAuthority(GetComponent<NetworkIdentity>().connectionToClient);
            }

        }

        private void initCharactersForHost()
        {
            gameServer.AddTeam();
            gameServer.AddTeam();

            FighterController[] foundCharacters = GameObject.FindObjectsOfType<FighterController>();
            foreach (FighterController character in foundCharacters)
            {
                int team = character.team;
                var newLogicCharacter = new GameServer.Character()
                {
                    maxLife = character.maxLife,
                    currentLife = character.maxLife,
                    movementDistance = character.range,
                };
                gameServer.teams[team].characters.Add(newLogicCharacter);
                var pos = character.getPosition2D();
                gameServer.map.cells[(int)pos.x, (int)pos.y].Object = newLogicCharacter;
                characters.Add(character.id, character);
                logicCharacters.Add(character.id, newLogicCharacter);
            }
        }
        private void initCharactersForClient()
        {
            FighterController[] foundCharacters = GameObject.FindObjectsOfType<FighterController>();
            foreach (FighterController character in foundCharacters)
            {
                characters.Add(character.id, character);
            }
        }

        private void initTiles()
        {
            TileObj[] tiles = GameObject.FindObjectsOfType<TileObj>();
            foreach (TileObj tile in tiles)
            {
                Vector2 pos = tile.getPosition2D();
                gameServer.map.enableCell((int)pos.x, (int)pos.y);
            }
        }

        [ClientRpc]
        private void RpcSendCharacter(GameServer.IntVec2[] path, int characterId)
        {
            for (int i = 0; i < path.Length; i++)
            {
                var step = new Vector3((float)path[i].x, 0, (float)path[i].y);
                var Path = characters[characterId].Path;
                Path.Add(step);
            }
            characters[characterId].walking = true;
        }

        public void CmdSelectTile(int x, int z)
        {

            if (moveState)
            {
                var pos = characters[selectedCharacter].getPosition2D();
                var from = new GameServer.IntVec2((int)pos.x, (int)pos.y);
                var to = new GameServer.IntVec2(x, z);
                gameServer.map.cells[from.x, from.y].Object = null;
                var path = gameServer.map.GetPath(from, to, logicCharacters[selectedCharacter].movementDistance);
                if (path.Length > 0)
                {
                    RpcSendCharacter(path, selectedCharacter);
                    gameServer.map.cells[to.x, to.y].Object = logicCharacters[selectedCharacter];
                }
                else
                {
                    gameServer.map.cells[from.x, from.y].Object = logicCharacters[selectedCharacter];
                }
                selectedCharacter = -1;
                selectCharState = true;
                moveState = false;
            }
        }

        public void CmdSelectChar(int characterId)
        {
            if (selectCharState)
            {
                selectCharState = false;
                moveState = true;

                selectedCharacter = characterId;
            }
        }

        public void SaveGameState()
        {
            SaveCharacterList characterSaves = new SaveCharacterList();

            foreach (KeyValuePair<int, FighterController> characterData in characters)
            {
                characterSaves.save.Add(new SaveCharacterInfo(characterData.Value.getPosition2D(), characterData.Key));
                Debug.Log(characterData.Value.getPosition2D() + " " + characterData.Key);
            }

            PlayerPrefs.SetString("saveData", JsonUtility.ToJson(characterSaves));
        }

        public void LoadGameState()
        {
            if (PlayerPrefs.HasKey("saveData"))
            {
                Debug.Log(PlayerPrefs.GetString("saveData"));
                var save = JsonUtility.FromJson<SaveCharacterList>(PlayerPrefs.GetString("saveData"));
                foreach (var savedChar in save.save)
                {
                    var fighterController = characters[savedChar.id];

                    fighterController.Path.Clear();
                    fighterController.Path.Add(new Vector3(savedChar.position.x, 0, savedChar.position.y));
                    fighterController.walking = true;
                }
            }
        }
    }
}