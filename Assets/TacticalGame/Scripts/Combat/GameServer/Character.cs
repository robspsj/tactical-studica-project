﻿using System.Collections;
using System.Collections.Generic;

namespace GameServer {
    public class Character : GridObject {
        public int maxLife;
        public int currentLife;
        public int movementDistance;
        public int attackValue;
        public AttackAction attackAction;
    }
}