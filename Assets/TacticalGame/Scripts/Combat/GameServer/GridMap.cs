﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GameServer {
    public class GridMap {
        public MapCell[, ] cells;
        int sizeX;
        int sizeY;

        public string debug = String.Empty;
        public GridMap (int _sizeX, int _sizeY) {
            sizeX = _sizeX;
            sizeY = _sizeY;

            cells = new MapCell[sizeX, sizeY];
            for (int x = 0; x < sizeX; x++) {
                for (int y = 0; y < sizeY; y++) {
                    cells[x, y] = new MapCell () { valid = false };
                }
            }
        }
        public void enableCell (int x, int y) {
            if (x < 0 || (x >= cells.GetLength (1))) {
                return;
            }
            if (y < 0 || (x >= cells.GetLength (0))) {
                return;
            }
            cells[x, y].valid = true;
        }

        private int[, ] CreatePathFindBase () {
            var pathFindBase = new int[sizeX, sizeY];

            for (int x = 0; x < sizeX; x++) {
                for (int y = 0; y < sizeY; y++) {
                    if (cells[x, y].valid && cells[x, y].Object == null) {
                        pathFindBase[x, y] = -1;
                    } else {
                        pathFindBase[x, y] = -2;
                    }
                }
            }
            pathFindBase.Initialize ();

            return pathFindBase;
        }

        public void setCellPathValue (int[, ] pathFindBase, int x, int y, int value) {
            if (value == 0) {
                pathFindBase[x, y] = value;
            }
            if (x < 0 || (x >= cells.GetLength (0))) {
                return;
            }
            if (y < 0 || (y >= cells.GetLength (1))) {
                return;
            }
            if ((pathFindBase[x, y] < value) && (pathFindBase[x, y] != -1)) {
                return;
            }

            pathFindBase[x, y] = value;
        }

        private bool StepPath (int[, ] pathFindBase, int step, IntVec2 to) {
            for (int x = 0; x < cells.GetLength (0); x++) {
                for (int y = 0; y < cells.GetLength (1); y++) {
                    if (pathFindBase[x, y] == step - 1) {
                        setCellPathValue (pathFindBase, x + 1, y, step);
                        setCellPathValue (pathFindBase, x - 1, y, step);
                        setCellPathValue (pathFindBase, x, y + 1, step);
                        setCellPathValue (pathFindBase, x, y - 1, step);
                        if (to.x == x && to.y == y) {
                            return true;

                        }
                    }
                }
            }
            return false;
        }

        private int getCellPathValue (int[, ] pathFindBase, int x, int y) {
            if (x < 0 || (x >= pathFindBase.GetLength (0))) {
                return -1;
            }
            if (y < 0 || (y >= pathFindBase.GetLength (1))) {
                return -1;
            }

            return pathFindBase[x, y];
        }
        public IntVec2 FindSmallerStepAround (int[, ] pathFindBase, IntVec2 pos) {
            int targetValue = getCellPathValue (pathFindBase, pos.x, pos.y) - 1;
            if (targetValue < 0) {
                return new IntVec2 (pos.x, pos.y);
            }
            if (getCellPathValue (pathFindBase, pos.x + 1, pos.y) == targetValue)
                return new IntVec2 (pos.x + 1, pos.y);
            if (getCellPathValue (pathFindBase, pos.x - 1, pos.y) == targetValue)
                return new IntVec2 (pos.x - 1, pos.y);
            if (getCellPathValue (pathFindBase, pos.x, pos.y + 1) == targetValue)
                return new IntVec2 (pos.x, pos.y + 1);
            else
                return new IntVec2 (pos.x, pos.y - 1);
        }
        public IntVec2[] GetPath (IntVec2 to, IntVec2 from, int maxLength) {
            List<IntVec2> path = new List<IntVec2> ();

            int[, ] pathFindBase = CreatePathFindBase ();

            setCellPathValue (pathFindBase, from.x, from.y, 0);

            bool found = false;
            for (int step = 1; step <= maxLength + 1; step++) {
                found = StepPath (pathFindBase, step, to);
                if (found)
                    break;
            }

            if (found) {
                var nextStep = to;
                for (int step = 1; step <= maxLength; step++) {
                    nextStep = FindSmallerStepAround (pathFindBase, nextStep);
                    if (getCellPathValue (pathFindBase, nextStep.x, nextStep.y) >= 0) {
                        path.Add (nextStep);
                    } else {
                        break;
                    }
                }
            }

            return path.ToArray ();
        }

    }
}