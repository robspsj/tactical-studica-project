namespace GameServer {

    public class IntVec2 {
        public IntVec2 (int _x, int _y) {
            x = _x;
            y = _y;
        }

        public IntVec2 () { }
        public int x;
        public int y;
    }
}