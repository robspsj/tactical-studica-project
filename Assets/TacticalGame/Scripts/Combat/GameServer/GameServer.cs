﻿using System.Collections;
using System.Collections.Generic;
namespace GameServer {
	public class GameServer {
		public int currentTurn;
		public List<Team> teams;
		public GridMap map;

		public GameServer (int mapWidth, int mapHeight) {
			map = new GridMap (mapWidth, mapHeight);
			teams = new List<Team> ();
		}

		public void AddTeam () {
			teams.Add (new Team ());
		}
		public void InitDefault () {
			for (int x = 0; x < 5; x++) {
				for (int y = 0; y < 5; y++) {
					map.enableCell (x, y);
				}
			}

			teams = new List<Team> ();

			teams.Add (new Team ());
			teams[0].characters.Add (new Character () {
				position = new IntVec2 () { x = 4, y = 1 },
					maxLife = 10,
					currentLife = 10,
					movementDistance = 3,
					attackAction = new AttackAction () {
						position = new IntVec2 () { x = 0, y = 1 }
					}
			});
			map.cells[4, 1].Object = teams[0].characters[0];

			teams.Add (new Team ());
			teams[1].characters.Add (new Character () {
				position = new IntVec2 () { x = 4, y = 8 },
					maxLife = 10,
					currentLife = 10,
					movementDistance = 3,
					attackAction = new AttackAction () {
						position = new IntVec2 () { x = 0, y = 1 }
					}
			});
			map.cells[4, 8].Object = teams[1].characters[0];

		}

	}
}