﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TacticalGame {
    public class FighterController : MonoBehaviour {
        public int team;
        public ClientPlayerController clientController;
        public List<Vector3> Path;
        public Vector3 front;

        public int maxLife = 10;
        public int range = 3;
        public int attackValue = 3;
        public int id;
        public bool walking = true;

        private void Start () {
            Path = new List<Vector3> ();
        }

        private void WalkPath () {
            if (Path.ToArray ().Length == 0) {
                walking = false;
                transform.rotation = Quaternion.Euler (front);
            } else {
                float speed = 3.0f;
                float dt = Time.deltaTime;

                transform.LookAt (Path[0] - (transform.localPosition - transform.position), Vector3.up);
                transform.localPosition = Vector3.MoveTowards (transform.localPosition, Path[0], speed * dt);
                if (transform.localPosition == Path[0]) {
                    Path.RemoveAt (0);
                }
            }
        }

        private void Update () {
            if (walking) {
                WalkPath ();
            }
        }

        public Vector2 getPosition2D () {
            return new Vector2 (transform.localPosition.x, transform.localPosition.z);
        }

        private void OnMouseUpAsButton () {
            clientController.receiveCharacterInput ((int) transform.position.x, (int) transform.position.z, id);
        }

        public void select () {
            transform.localScale = new Vector3 (1.0f, 1.3f, 1.0f);
        }
        public void desselect () {
            transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
        }
    }
}