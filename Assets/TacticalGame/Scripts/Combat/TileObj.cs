﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TacticalGame {
    public class TileObj : MonoBehaviour {
        public Material focusedMaterial;
        public Material unfocusedMaterial;
        MeshRenderer meshRenderer;
        public ClientPlayerController clientController;

        void Start () {
            meshRenderer = GetComponent<MeshRenderer> ();
        }

        void Update () { }

        private void OnMouseOver () {
            meshRenderer.material = focusedMaterial;
        }

        private void OnMouseExit () {
            meshRenderer.material = unfocusedMaterial;
        }

        public Vector2 getPosition2D () {
            return new Vector2 (transform.position.x, transform.position.z);
        }

        private void OnMouseUpAsButton () {
            clientController.receiveTileInput ((int) transform.position.x, (int) transform.position.z);
        }
    }
}