﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace TacticalGame
{

    public class ClientPlayerController : NetworkBehaviour
    {

        Dictionary<int, FighterController> characters = new Dictionary<int, FighterController>();

        [SyncVar]
        int selectedCharacter = -1;

        private void Start()
        {
            if (isLocalPlayer)
            {
                initCharactersForClient();
				initTiles();
				Debug.Log("initialized");
            }
        }

        private void initCharactersForClient()
        {
            FighterController[] foundCharacters = GameObject.FindObjectsOfType<FighterController>();
            foreach (FighterController character in foundCharacters)
            {
                characters.Add(character.id, character);
				character.clientController = this;
            }
        }

		private void initTiles()
        {
            TileObj[] tiles = GameObject.FindObjectsOfType<TileObj>();
            foreach (TileObj tile in tiles)
            {
                tile.clientController = this;
            }
        }

		GameController GetGameController () {
            var gameControllers = GameObject.FindGameObjectsWithTag ("GameController");
            if (gameControllers.Length == 0) {
                return null;
            }
            return  gameControllers[0].GetComponent<GameController> ();
        }

        [Command]
        public void CmdSelectTile(int x, int z)
        {
			GetGameController().CmdSelectTile(x, z);
        }

        [Command]
        public void CmdSelectChar(int characterId)
        {
			GetGameController().CmdSelectChar(characterId);
        }

        public void receiveTileInput(int x, int z)
        {
            if (isLocalPlayer)
            {
                if (selectedCharacter != -1)
                {
                    characters[selectedCharacter].desselect();
                }
                CmdSelectTile(x, z);
            }
        }
        public void receiveCharacterInput(int x, int z, int characterId)
        {
            if (isLocalPlayer)
            {
                if (selectedCharacter != -1)

                    characters[selectedCharacter].desselect();
                CmdSelectChar(characterId);
                characters[characterId].select();
            }
        }
    }
}
